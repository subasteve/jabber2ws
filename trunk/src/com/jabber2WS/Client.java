package com.jabber2WS;

import com.master.io.interfaces.BufferListener;
import com.master.io.socket.Buffer;
import com.master.io.socket.ClientSocket;
import com.master.util.Logger;
import com.master.util.SearchableString;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;

import javax.net.ssl.*;
import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.Executors;

/**
 * Created by subasteve on 10/27/14.
 */
public class Client implements BufferListener {

    private final SearchableString STRING = new SearchableString();

    private boolean handshakeComplete = false;
    private ClientSocket SOCKET;
    private int listenerID = -1;
    private WebSocket WEB_SOCKET;
    private SSLEngine engine = null;
    private final String HOST;
    private final int PORT;
    private final Logger LOGGER;

    public Client(final WebSocket WEB_SOCKET, final String HOST, final int PORT, final Logger LOGGER) throws IOException{
        this.LOGGER = LOGGER;
        this.WEB_SOCKET = WEB_SOCKET;
        this.HOST = HOST;
        this.PORT = PORT;
        SOCKET = new ClientSocket(HOST, PORT);
        listenerID = SOCKET.put(this);
    }

    public final void destruct() throws ClosedChannelException{
        SOCKET.getOutBuffer().reset();
        SOCKET.getOutBuffer().put("</stream:stream>".getBytes());
        SOCKET.getOutBuffer().putToChannel(LOGGER);
        SOCKET.getOutBuffer().reset();
        handshakeComplete = false;
        if(engine != null){
            engine.closeOutbound();
            engine = null;
        }
        if(listenerID != -1){
            SOCKET.remove(listenerID);
        }
        SOCKET.destruct();
    }

    public final void forwardData(final String STRING){
        try{
            SOCKET.getOutBuffer().reset();
            if(engine != null && handshakeComplete){
                LOGGER.writeLine("SSL["+engine.getHandshakeStatus().toString()+"] onMessage: "+STRING);
                SOCKET.getTmpBuffer().put(STRING.getBytes());
                SOCKET.getTmpBuffer().flip();
                //Must flip before
                runDelegatedTasks(engine.wrap(SOCKET.getTmpBuffer().getByteBuffer(), SOCKET.getOutBuffer().getByteBuffer()), engine);
                SOCKET.getTmpBuffer().reset();
                SOCKET.getOutBuffer().putToChannel(LOGGER);
            }else{
                LOGGER.writeLine("onMessage: "+STRING);
                SOCKET.getOutBuffer().put(STRING.getBytes());
                SOCKET.getOutBuffer().putToChannel(LOGGER);
            }
            SOCKET.getOutBuffer().reset();
        }catch (final Exception E){
            LOGGER.writeError(E);
        }
    }

    @Override
    public Buffer bufferSend(final Buffer BUFFER_OUT){
        return BUFFER_OUT;
    }

    private void runDelegatedTasks(SSLEngineResult result,
                                          SSLEngine engine) throws Exception {

        if (result.getHandshakeStatus() == SSLEngineResult.HandshakeStatus.NEED_TASK) {
            Runnable task = engine.getDelegatedTask();
            if(task != null){
                Executors.newSingleThreadExecutor().execute(task);
            }
            SSLEngineResult.HandshakeStatus hsStatus = engine.getHandshakeStatus();
            if (hsStatus == SSLEngineResult.HandshakeStatus.NEED_TASK) {
                throw new Exception(
                        "handshake shouldn't need additional tasks");
            }
            LOGGER.writeLine("new HandshakeStatus: " + hsStatus);
        }
        LOGGER.writeLine("SSL Status "+result.getStatus());
    }

    @Override
    public Buffer bufferRecieve(final Buffer BUFFER_IN){
        if(engine != null && handshakeComplete){
            try {
                final Buffer BUFFER = new Buffer(engine.getSession().getApplicationBufferSize());
                BUFFER_IN.flip();
                //Must flip before
                while(BUFFER_IN.position() != BUFFER_IN.getByteBuffer().limit()){
                    runDelegatedTasks(engine.unwrap(BUFFER_IN.getByteBuffer(), BUFFER.getByteBuffer()), engine);
                }
                STRING.setBuffer(BUFFER.getByteBuffer());
                LOGGER.writeLine("SSL["+engine.getHandshakeStatus().toString()+"] bufferRecieve");
            }catch (final Exception E){
                LOGGER.writeError(E);
            }
        }else {
            LOGGER.writeLine("bufferRecieve");
            STRING.setBuffer(BUFFER_IN.getByteBuffer());
        }
        final String RESPONSE = STRING.position(0).toString();
        //Handle TLS
        if(RESPONSE.startsWith("<proceed ")) {
            try {
                final SSLContext CONTEXT = SSLContext.getInstance("TLSv1");
                X509TrustManager tm = new X509TrustManager() {

                    public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };
                CONTEXT.init(null, new TrustManager[]{tm}, null);
                engine = CONTEXT.createSSLEngine(HOST,PORT);
                engine.setUseClientMode(true);
                engine.beginHandshake();
                SSLEngineResult.HandshakeStatus hs = engine.getHandshakeStatus();

                while (hs != SSLEngineResult.HandshakeStatus.FINISHED &&
                        hs != SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING && !handshakeComplete) {

                    if(hs == SSLEngineResult.HandshakeStatus.NEED_UNWRAP){
                        int read=1;
                        while (read > 0) {
                            read=SOCKET.getSocket().read(SOCKET.getTmpBuffer().getByteBuffer());
                            if(read==-1){
                                throw new IOException ("channel closed");
                            }
                        }

                        SOCKET.getTmpBuffer().getByteBuffer().flip();
                        final SSLEngineResult engineRes = engine.unwrap(SOCKET.getTmpBuffer().getByteBuffer(), SOCKET.getInBuffer().reset(SOCKET.getTmpBuffer().getByteBuffer().limit()).getByteBuffer());
                        SOCKET.getTmpBuffer().getByteBuffer().compact();

                        //LOGGER.writeLine("SSL NEED_UNWRAP Handshake["+engineRes.getStatus().toString()+"]");

                        switch(engineRes.getStatus()){
                            case BUFFER_OVERFLOW:
                                //System.out.println("overFlow");
                                break;
                            case BUFFER_UNDERFLOW:
                                //System.out.println("underFlowa");
                                break;
                            case CLOSED:
                                break;
                            case OK:
                                break;
                            default:
                                break;
                        }

                    }else if(hs == SSLEngineResult.HandshakeStatus.NEED_WRAP){
                        SOCKET.getOutBuffer().getByteBuffer().clear();
                        final SSLEngineResult engineRes = engine.wrap(SOCKET.getInBuffer().getByteBuffer(), SOCKET.getOutBuffer().getByteBuffer());
                        SOCKET.getOutBuffer().getByteBuffer().flip();
                        LOGGER.writeLine("SSL NEED_WRAP Handshake["+engineRes.getStatus().toString()+"]");
                        switch (engineRes.getStatus()){
                            case BUFFER_OVERFLOW:
                                System.out.println("overFlow");
                                break;
                            case BUFFER_UNDERFLOW:
                                System.out.println("underFlow");
                                break;
                            case CLOSED:
                                break;
                            case OK:
                                while(SOCKET.getOutBuffer().getByteBuffer().hasRemaining()){
                                    if(SOCKET.getSocket().write(SOCKET.getOutBuffer().getByteBuffer())<0){
                                        throw new Exception("Channel Has been Closed");
                                    }
                                }

                                break;
                            default:
                                break;


                        }
                    }else if(hs == SSLEngineResult.HandshakeStatus.NEED_TASK){
                        Runnable task = engine.getDelegatedTask();
                        if(task != null){
                            Executors.newSingleThreadExecutor().execute(task);
                        }
                    }

                    hs = engine.getHandshakeStatus();

                }
            }catch (final Exception E){
                LOGGER.writeError(E);
            }
            LOGGER.writeLine("SSL handshake complete");
            handshakeComplete = true;
        }
        final String TRUNCATED = RESPONSE.substring(0,RESPONSE.lastIndexOf(">")+1);
        LOGGER.writeLine(TRUNCATED);
        WEB_SOCKET.send(TRUNCATED);
        return (engine == null) ? BUFFER_IN.reset(1048576) : BUFFER_IN.reset(engine.getSession().getApplicationBufferSize());
    }

}
