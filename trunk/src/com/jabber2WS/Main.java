package com.jabber2WS;

import com.master.util.LoadIni;
import com.master.util.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Main extends WebSocketServer {

    private static final Map<Integer, Client> CLIENTS = new HashMap<Integer, Client>();
    private static final LoadIni INI = new LoadIni("config.ini");
    private static final Logger LOGGER = new Logger("./logs/Jabber.log",true,false);

    private long connection = 0;

    public static final void main(final String[] ARGS) throws IOException {
        new Main();
    }

    public Main() throws IOException {
        super(new InetSocketAddress(getWSPort()), Collections.singletonList((Draft) new Draft_17()));
        LOGGER.writeLine("Jabber: " + getHost() + ":" + getJabberPort());
        WebSocketImpl.DEBUG = false;
        start();
    }

    public static int getJabberPort(){
        return INI.loadInt("port");
    }

    public static int getWSPort(){
        return INI.loadInt("wsPort");
    }

    public static String getHost(){
        return INI.loadString("host");
    }

    @Override
    public void onOpen(final WebSocket SOCKET, final ClientHandshake HANDSHAKE) {
        connection++;
        try {
            CLIENTS.put(SOCKET.hashCode(), new Client(SOCKET,getHost(),getJabberPort(),LOGGER));
        }catch (final Exception E){
            LOGGER.writeError(E);
        }
    }

    @Override
    public void onClose(final WebSocket SOCKET, int i, String s, boolean b) {
        connection--;
        final Client CLIENT = CLIENTS.get(SOCKET.hashCode());
        if(CLIENT != null){
            try {
                CLIENT.destruct();
            }catch (final Exception E){
                LOGGER.writeError(E);
            }
            CLIENTS.remove(SOCKET.hashCode());
        }
    }

    @Override
    public void onMessage(final WebSocket SOCKET, final String STRING){
        final Client CLIENT = CLIENTS.get(SOCKET.hashCode());
        if(CLIENT != null){
            try{
                CLIENT.forwardData(STRING);
            }catch (final Exception E){
                LOGGER.writeError(E);
            }
        }
    }

    @Override
    public void onError(final WebSocket SOCKET, final Exception E) {
        LOGGER.writeError(E);
    }

}
