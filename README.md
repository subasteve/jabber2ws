Jabber2WS
========================================
Prototype not tested for production  
Websocket -> Jabber2WS -> jabber server

Realtime jabber [xmpp protocol](http://en.wikipedia.org/wiki/XMPP) to browsers that leaves the protocol handling to the javascript client implementing the websocket.

Prerequisites
-------------
1. Apache ant
2. Browser with websocket support

Dependencies
-------------
Dependencies are git submodules where possible. Otherwise the jar can just be included into Lib. SQL packages need to be included at runtime for that functionality to work. Also go into depends folder and use init & update per project.

#### Init subModules ####

```
#!sh
git submodule init
```

#### Update subModules ####
```
#!sh
git submodule update
```
```
#!sh
git submodule foreach git pull origin master
```

Compile
-------------
```
#!sh
ant
```

Test
-------------
```
#!sh
ant test
```